/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//kode program diatas adalah untuk membuat jendela dialog JFrame dengan tekniK Hard Coding.
package tigaA;
import javax.swing.JFrame; //fungsinya karena kita membutuhkan class JFrame, maka kita harus mengimpornya dari package javax.swing.
public class Latihan1 {// mendeklarasikan class baru bernama Newclass
    public static void main(String[] args) {//membuat metode utama agar program dapat dijalankan
        JFrame frame = new JFrame ("Ini Frame pertamaku");//membuat jendela objek dari class Jframe dan memberi judul
        
        int tinggi = 400; //digunakan untuk mengatur properti jendela berupa tinggi dan lebar dari JFramenya
        int lebar = 400; // atau menentukan ukuran frame
        
        frame.setBounds(100, 100, lebar, tinggi); //ntuk menentukan posisi label di dalam frame beserta width dan height.
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);// untuk mengakhiri jalannya program bila frame ditutup. bila perintah tersebut tidak ada maka penutupan frame tidak akan menghentikan jalannya program
        frame.setVisible(true);//Mengatur Class menjadi Terlihat = True / Aktifkan Visible Frame
    }
}

//
